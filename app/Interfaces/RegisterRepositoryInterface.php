<?php

    namespace App\Interfaces;

    interface RegisterRepositoryInterface
    {
        public function create($array);
    }

?>