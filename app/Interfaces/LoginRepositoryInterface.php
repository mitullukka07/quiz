<?php

    namespace App\Interfaces;

    interface LoginRepositoryInterface
    {
        public function login($array);
        public function logout();
    }

?>