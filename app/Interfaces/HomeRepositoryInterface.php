<?php

    namespace App\Interfaces;

    interface HomeRepositoryInterface
    {
        public function store($array); 
        public function result($array);   
        public function subject($array);
        public function showMCQ($array);    
        public function showresult($array);
    }

?>