<?php

    namespace App\Interfaces;

    interface SubjectRepositoryInterface
    {
        public function store($array);
        public function update($array);
        public function show($id);
        
    }

?>