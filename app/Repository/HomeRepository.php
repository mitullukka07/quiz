<?php
namespace App\Repository;

use App\Interfaces\HomeRepositoryInterface;
use App\Models\Question;
use App\Models\Result;
use Illuminate\Support\Facades\DB;

class  HomeRepository implements HomeRepositoryInterface
{
    public function store($array)
    {
        // dd($array['answer_id']);
        // $ans = @json_decode(json_encode($array['answer_id']),true);        
        $arr = $array->all();              
        $user = request()->user()->id;        
        foreach ($arr['answer_id'] as $key=>$data) {            
            Result::create([
                'user_id' => $user,
                'question_id' => $key ,
                'answer_id' => $data,       
            ]);
        }
        return response()->json('1'); 
    }

    public function result($array)
    {
        $question = Question::count();
        $result = Result::get();
        $user_id = request()->user()->id;
        $right = [];
        $attempt_questions = Result::where('user_id', $user_id)->get();
        $total_correct = 0;
        foreach ($attempt_questions as $key => $answer) {
            $is_correct = Question::where('id', $answer->question_id)->where('right_answer', $answer->answer_id)->first();
            $is_correct ? ++$total_correct : "";
        }
        $count = $question;
        $question = $attempt_questions->count();
        $right1 = $total_correct;
        return view('result.index', compact('count', 'question', 'right1'));
    }

    public function showresult($array)
    {
        $question = Question::count();
        $result = Result::get();
        $user_id = request()->user()->id;
        $right = [];
        $attempt_questions = Result::where('user_id', $user_id)->get();
        $total_correct = 0;
        foreach ($attempt_questions as $key => $answer) {
            $is_correct = Question::where('id', $answer->question_id)->where('right_answer', $answer->answer_id)->first();
            $is_correct ? ++$total_correct : "";
        }
        $count = $question;
        $question = $attempt_questions->count();
        $right1 = $total_correct;
        return response()->json(['count' => $count, 'question' => $question, 'right' => $right1]);
    }

    public function subject($array)
    {
        $question_id = array_unique($array->question_id);
        $subject = Question::where('subject_id', $array->subject_id)->first();
        $find = Question::where('subject_id', $subject->subject_id)->get();
        return view('home', compact('find'));
    }

    public function showMCQ($array)
    {    
        $subject = Question::where('subject_id',$array['subject_id'])->first();
        $find = Question::with('answers')->where('subject_id',$subject->subject_id)->get(); 
        return $find;
    }
}
?>

