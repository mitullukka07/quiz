<?php
namespace App\Repository;

use App\Interfaces\SubjectRepositoryInterface;
use App\Models\Subject;

class  SubjectRepository implements SubjectRepositoryInterface
{
    public function store($array)
    {
        $question = new Subject;
        $question->subject   =    $array->subject;
        $question->save();
        return $question;
    }

    public function update($array)
    {
        $subject = Subject::find($array->id);
        $subject->subject = $array->subject;
        $subject->save();
        return $subject;
    }

    public function show($id)
    {
        $subject = Subject::select('id', 'subject')->find($id);
        return response()->json(['subject' => $subject]);
    }
}
?>



