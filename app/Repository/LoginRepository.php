<?php
namespace App\Repository;

use App\Interfaces\LoginRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class  LoginRepository implements LoginRepositoryInterface
{
    public $successStatus = 200;
    public function login($array)
    {
        $credentials = $array->only('email', 'password');
        if (Auth::guard()->attempt($credentials)) {
            $user = Auth::guard()->user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function logout()
    {   
        $token = auth()->user()->token();
        $token->revoke();
        return response(['message' => 'You have been successfully logged out.'], 200);

    }

}
?>



