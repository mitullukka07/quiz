<?php
namespace App\Repository;

use App\Interfaces\RegisterRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class  RegisterRepository implements RegisterRepositoryInterface
{
    public function create($array)
    {
        return User::create([
            'name' => $array['name'],
            'email' => $array['email'],
            'password' => Hash::make($array['password']),
        ]);
        return response(['message' => 'You have been successfully register ']);
    }
}
?>



