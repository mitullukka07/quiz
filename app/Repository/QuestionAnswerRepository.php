<?php
namespace App\Repository;

use App\Interfaces\QuestionAnswerRepositoryInterface;
use App\Models\Answer;
use App\Models\Question;
class  QuestionAnswerRepository implements QuestionAnswerRepositoryInterface
{
    public function store($array)
    {
        $question = new Question();
        $question->title   =     $array->title;
        $question->right_answer = $array->correct_answer;
        $question->subject_id = $array->question_id;
        $question->save();

        foreach ($array['option'] as $data) {
            Answer::create([
                'question_id' => $question->id,
                'answer' => $data,
                'right_answer' => $array->answer
            ]);
        }
        return response()->json('1');
    }
}
?>



