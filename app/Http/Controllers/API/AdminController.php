<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::guard('admin')->attempt($credentials)) {
            $user = Auth::guard('admin')->user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
    
    public function Logout()
    {
        $token = Auth::user()->token();
        $token->revoke();
        return response(['message' => 'You have been successfully logged out.'], 200);
    }
}
