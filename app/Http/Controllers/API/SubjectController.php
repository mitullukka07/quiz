<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repository\SubjectRepository;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class SubjectController extends Controller
{
    public function __construct(SubjectRepository $subjectRepository)
    {
        $this->subjectRepository = $subjectRepository; 
    }

    public function store(Request $request)
    {
        return $this->subjectRepository->store($request);
    }

    public function update(Request $request)
    {
        return $this->subjectRepository->update($request);
    }

    public function show($id)
    {
        return $this->subjectRepository->show($id);
    }

    
   
}
