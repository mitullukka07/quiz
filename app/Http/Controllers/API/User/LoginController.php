<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Repository\LoginRepository;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }

    public function userlogin(Request $request)
    {
        return $this->loginRepository->login($request); 
    }

    public function logout(Request $request)
    {
        return $this->loginRepository->logout($request); 
    }

}
