<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Repository\HomeRepository;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class HomeController extends Controller
{
    public function __construct(HomeRepository $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }

    public function store(Request $request)
    {
        return $this->homeRepository->store($request); 
    }
    
    public function result(Request $request)
    {
        return $this->homeRepository->showresult($request); 
    }

    public function subject(Request $request)
    {
        $showMCQ  = $this->homeRepository->showMCQ($request->all());
        return response()->json($showMCQ);
    }

    public function showresult(Request $request)
    {
        $showMCQ  = $this->homeRepository->showresult($request->all());
        return response()->json($showMCQ);
    }
}
