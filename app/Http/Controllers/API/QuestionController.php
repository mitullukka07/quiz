<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repository\QuestionAnswerRepository;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct(QuestionAnswerRepository $questionanswerRepository)
    {
        $this->questionanswerRepository = $questionanswerRepository; 
    }
   
    public function store(Request $request)
    {
        return $this->questionanswerRepository->store($request);
    }
}
