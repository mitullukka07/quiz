<?php

namespace App\Http\Controllers;

use App\DataTables\ResultDataTable;
use App\DataTables\UserDataTable;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Result;
use App\Repository\HomeRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct(HomeRepository $homeRepository)
    {
        $this->homeRepository = $homeRepository;
    }
    
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index(Request $request)
    {       
        return view('home');
    }

    public function show(ResultDataTable $resultDataTable)
    {
        return $resultDataTable->render('admin.result');
    }

    public function store(Request $request)
    {                  
        return $this->homeRepository->store($request);  
    }

    public function result(Request $request)
    {

        return $this->homeRepository->result($request); 
        // $question = Question::count();  
        // $result = Result::get();
        // $user_id = request()->user()->id;
        // $right= []; 
        // $attempt_questions = Result::where('user_id',$user_id)->get();
        // $total_correct = 0;                                   
        // foreach($attempt_questions as $key => $answer)
        // {
        //     $is_correct = Question::where('id',$answer->question_id)->where('right_answer',$answer->answer_id)->first();
        //     $is_correct ? ++$total_correct : "";
        // }         
        // $count = $question;
        // $question = $attempt_questions->count();
        // $right1 = $total_correct;        
        // return view('result.index',compact('count','question','right1'));    
    }

    public function subjectshow(Request $request)
    {        
        return view('subject');
    }

    public function subject(Request $request)
    {  
        return $this->homeRepository->subject($request); 
        // $question_id = array_unique($request->question_id);
        // $subject = Question::where('subject_id',$request->subject_id)->first();
        // $find = Question::where('subject_id',$subject->subject_id)->get();  
        // return view('home',compact('find'));
    }
}



