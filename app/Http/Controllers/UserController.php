<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Models\Question;
use App\Models\Result;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('user.index');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return response()->json(['user'=>$user]);
    }

    public function sendmail(Request $request)
    {       
        $user = new User;
        $usr = User::where('id',$request->id)->first();
        $data = ['name' => $user];
        $user['to'] = $usr->email;
        Mail::send('mails.mail', $data, function ($message) use ($user) {
            $message->to($user['to']);
            $message->subject('Hello');
        });        
        return response()->json(['user'=>$user]); 
    }

    public function result(Request $request)
    {        
        $question = Question::all()->groupBy('id')->count();
        $q = Question::all();
        foreach($q as $q1)
        {
            $count = Result::all()->groupBy('answer_id')->count();
            $right = Result::all()->where('answer_id',$q1->right_answer)->count();            
        }       
        return view('result.index',compact('count','question','right'));    
    }
}
