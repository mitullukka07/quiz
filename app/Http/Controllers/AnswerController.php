<?php

namespace App\Http\Controllers;

use App\DataTables\AnswerDataTable;
use App\Interfaces\QuestionAnswerRepositoryInterface;
use App\Models\Answer;
use App\Models\Question;
use App\Repository\QuestionAnswerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnswerController extends Controller
{
    public $QuestionAnswerRepository;
    public function __construct(QuestionAnswerRepository $questionanswerRepository)
    {
        $this->questionanswerRepository = $questionanswerRepository;
    }
    
    public function index(AnswerDataTable $answerDataTable)
    {
        return $answerDataTable->render('answer.index');
    }

    public function create()
    {
        $question = Question::all();
        return view('answer.create',compact('question'));
    }

    public function store(Request $request)
    {
        return $this->questionanswerRepository->store($request); 
        // $question = new Question;
        // $question->title   =     $request->title;
        // $question->right_answer = $request->correct_answer;        
        // $question->save();

        // foreach ($request['option'] as $data) {
        //     Answer::create([
        //         'question_id' => $question->id,
        //         'answer' => $data,
        //         'right_answer' => $request->answer
        //     ]);
        // }
        // return response()->json('1');
    }

    public function show(Answer $answer)
    {
        //
    }

    public function edit(Answer $answer)
    {
        
    }

    public function update(Request $request, Answer $answer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        //
    }


}
