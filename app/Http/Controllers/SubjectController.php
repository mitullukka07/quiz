<?php

namespace App\Http\Controllers;

use App\DataTables\SubjectDataTable;
use App\Http\Requests\Subject\StoreRequest;
use App\Http\Requests\Subject\UpdateRequest;
use App\Models\Subject;
use App\Repository\SubjectRepository;
use Illuminate\Http\Request;

class SubjectController extends Controller
{

    public function __construct(SubjectRepository $subjectRepository)
    {
        $this->subjectRepository = $subjectRepository;
    }

    public function index(SubjectDataTable $subjectDataTable)
    {
        return $subjectDataTable->render('subject.index');
    }

    public function create()
    {
        return view('subject.create');
    }

    public function store(StoreRequest $request)
    {
        $store = $this->subjectRepository->store($request);
        return response()->json($store);
    }

    public function edit($id)
    {
        $subject = Subject::find($id);
        return response()->json(['subject'=>$subject]);
    }

    public function update(UpdateRequest $request, Subject $subject)
    {
        $update =  $this->subjectRepository->update($request);
        return response()->json(['update' => 'Success','property'=>$update]);
    }

    public function destroy(Subject $subject)
    {
        
    }
}
