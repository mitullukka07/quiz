<?php

namespace App\Http\Controllers;

use App\DataTables\QuestionDataTable;
use App\Http\Requests\Question\StoreRequest;
use App\Http\Requests\Question\UpdateRequest;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(QuestionDataTable $questionDataTable)
    {
        return $questionDataTable->render('question.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('question.create');
    }
    
    public function store(StoreRequest $request)
    {
        $question = new Question;
        $question->title   =     $request->title;
        $question->right_answer = $request->answer;
        $question->save();
        return response()->json(1);
    }

    public function show(Question $question)
    {
        //
    }

    public function edit($id)
    {
        $question = Question::find($id);
        return response()->json(['question'=>$question]);
    }

    public function update(UpdateRequest $request, Question $question)
    {
        $question = Question::find($request->id);
        $question->title = $request->title;
        $question->save();
        return response()->json(1);
    }

    public function destroy(Question $question)
    {
        //
    }
}
