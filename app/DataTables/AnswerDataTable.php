<?php

namespace App\DataTables;

use App\Models\Answer;
use App\Models\Question;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AnswerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $btn = "";
                $btn = '<a href="' . route('admin.question.edit', $data->id) . '" data-id="' . $data->id . '"    data-bs-backdrop="static" data-bs-keyboard="false"  data-bs-toggle="modal" data-bs-target="#myModal" class="edit btn btn-info btn-sm btnedit"><i class="fa fa-edit"></i></a>&nbsp';
                // $btn .= '<a href=""   class="edit btn btn-danger btn-sm "><i class="fa fa-trash"></i></a>&nbsp';
                return $btn;
            })->addColumn('question_id', function ($data) {
                $name = Question::where('id', $data->question_id)->first();
                return $name->title;
            })
            ->rawColumns(['action', 'question_id'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Answer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Answer $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('answer-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('question_id')->title('QuestionName'),
            Column::make('answer'),
            Column::computed('action')
            ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Answer_' . date('YmdHis');
    }
}
