<?php

namespace App\Providers;

use App\Interfaces\AdminRepositoryInterface;
use App\Interfaces\QuestionAnswerRepositoryInterface;

use App\Repository\QuestionAnswerRepository;
use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AdminRepositoryInterface::class,AdminRepository::class);
        $this->app->bind(QuestionAnswerRepositoryInterface::class,QuestionAnswerRepository::class);
    }
}
