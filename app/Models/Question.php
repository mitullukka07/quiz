<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Question extends Model
{
    use HasFactory,HasApiTokens;

    protected $fillable = [
        'title','right_answer','subject_id'
    ];

    protected $hidden =[
        'created_at','updated_at'
    ];

    public function answers()
    {
        return $this->hasMany(Answer::class,'question_id','id')->select('question_id','answer');
    }
}
