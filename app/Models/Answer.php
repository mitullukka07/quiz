<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Answer extends Model
{
    use HasFactory,HasApiTokens;

    protected $fillable = [
        'question_id','answer'
    ];

    protected $hidden =[
        'created_at','updated_at'
    ];

}
