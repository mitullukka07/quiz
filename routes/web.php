<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('subject', [App\Http\Controllers\HomeController::class, 'subjectshow'])->name('subject');
Route::post('subject', [App\Http\Controllers\HomeController::class, 'subject']);

Route::get('show',[App\Http\Controllers\HomeController::class, 'show'])->name('show');

Route::post('store',[App\Http\Controllers\HomeController::class, 'store'])->name('store');

Route::get('result',[App\Http\Controllers\HomeController::class, 'result'])->name('result');