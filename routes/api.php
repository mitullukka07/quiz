<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\QuestionController;
use App\Http\Controllers\API\SubjectController;
use App\Http\Controllers\API\User\HomeController;
use App\Http\Controllers\API\User\LoginController;
use App\Http\Controllers\API\User\RegisterController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[AdminController::class,'Login']);

Route::middleware('auth:api')->group(function () {  
    Route::get('logout', [AdminController::class, 'Logout']);
    Route::group(['prefix' => 'question', 'as' => 'question.'], function () {
        Route::post('store', [QuestionController::class, 'store'])->name('store');
        Route::post('update', [QuestionController::class, 'update'])->name('update');
    });

    Route::group(['prefix' => 'answer', 'as' => 'answer.'], function () {
        Route::post('store', [QuestionController::class, 'store'])->name('store');
        Route::post('update', [QuestionController::class, 'update'])->name('update');
    });

    Route::group(['prefix' => 'subject', 'as' => 'subject.'], function () {
        Route::post('store', [SubjectController::class, 'store'])->name('store');
        Route::post('update', [SubjectController::class, 'update'])->name('update');
    });
});

Route::post('create',[RegisterController::class,'create']);
Route::post('userlogin',[LoginController::class,'UserLogin']);

Route::middleware('auth:api_user')->group(function () {  
    Route::get('logout', [LoginController::class, 'Logout']); 
    
    Route::group(['prefix' => 'result', 'as' => 'result.'], function () {
        Route::post('store', [HomeController::class, 'store'])->name('store');
        Route::get('result', [HomeController::class, 'result'])->name('result');
        Route::get('subject', [HomeController::class, 'subject'])->name('subject');
        Route::get('showresult', [HomeController::class, 'showresult'])->name('showresult');
    });

    Route::group(['prefix'=>'subject','as'=>'subject.'],function(){
        Route::get('show/{id}',[SubjectController::class,'show'])->name('show');
    });



});