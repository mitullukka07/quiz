<?php

use App\DataTables\PropertyFeaturesDataTable;
use App\Http\Controllers\AnswerController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\SubjectController;

Route::group(['namespace' => 'Auth'], function () {
    # Login Routes
    Route::get('login', 'Admin\AdminController@showLoginForm')->name('login');
    Route::post('login', 'Admin\AdminController@login');
    Route::post('admin/logout', 'Admin\AdminController@logout')->name('logout');
});

Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');


Route::group(['prefix' => 'result', 'as' => 'result.'], function () {
    Route::get('show', [HomeController::class, 'show'])->name('show');    
});

Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('index', [UserController::class, 'index'])->name('index');
    Route::get('show/{id}', [UserController::class, 'show'])->name('show');
    Route::post('sendmail',[UserController::class, 'sendmail'])->name('sendmail');
    Route::get('result', [UserController::class, 'result'])->name('result');       
});
    
Route::group(['prefix'=>'question','as'=>'question.'],function(){
    Route::get('create',[QuestionController::class,'create'])->name('create');
    Route::post('store',[QuestionController::class,'store'])->name('store');
    Route::get('index',[QuestionController::class,'index'])->name('index');
    Route::get('edit/{id}',[QuestionController::class,'edit'])->name('edit');
    Route::post('update',[QuestionController::class,'update'])->name('update');
});

Route::group(['prefix'=>'answer','as'=>'answer.'],function(){
    Route::get('create',[AnswerController::class,'create'])->name('create');
    Route::post('store',[AnswerController::class,'store'])->name('store');
    Route::get('index',[AnswerController::class,'index'])->name('index');
    Route::get('edit/{id}',[AnswerController::class,'edit'])->name('edit');
    Route::post('update',[AnswerController::class,'update'])->name('update');
});

Route::group(['prefix'=>'subject','as'=>'subject.'],function(){
    Route::get('create',[SubjectController::class,'create'])->name('create');
    Route::post('store',[SubjectController::class,'store'])->name('store');
    Route::get('index',[SubjectController::class,'index'])->name('index');
    Route::get('edit/{id}',[SubjectController::class,'edit'])->name('edit');
    Route::post('update',[SubjectController::class,'update'])->name('update');
});
});
?>