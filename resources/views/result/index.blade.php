@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('MCQ Result') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <!-- {{ __('You are logged in!') }} -->

                    <form class="custom-validation" id="myform" method="post">
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <input type="hidden" name="user_id">
                            <div class="step-detail-box" >
                                <!-- <h4 class="step-main-head">MCQ Test</h4> -->
                                @php
                                     $data = DB::table('questions')->get();
                                @endphp
                                @foreach($data as $d)
                                        <input type="hidden" value="{{$d->right_answer}}" name="answer[]">
                                        <input type="hidden" value="{{$d->subject_id}}" name="subject_id">
                                @endforeach
                                <input type="hidden" value="{{$d->subject_id}}" name="subject_id">
                                <div class="filters-boxes" >                                   
                                    <div class="filter-box" >
                                        <div class="row" style="background-color:gray">
                                            Total question<span>{{$count}}</span>
                                        </div>

                                        <div class="row" >
                                            Attempt<span>{{$question}}</span>
                                        </div>
                                                                                 
                                        <div class="row" style="background-color:#98FB98">
                                            Right Answer<span>{{$right1}}</span>
                                        </div>
                                                                                 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>

</script>


@endpush