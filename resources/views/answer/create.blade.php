@extends('layouts.admin.master')
@section('title','Answer')
@section('content')
<!-- Begin page -->
<div id="layout-wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">Answer</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item">Answer</li>
                                    <li class="breadcrumb-item active">Answer</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <!-- end row -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">

                                <form class="custom-validation" id="myform" method="post">
                                    @csrf
                                    @php
                                        $data = DB::table('subjects')->get();
                                    @endphp
                                    <div class="mb-3">
                                        <label class="col-md-2 col-form-label">Subject</label>
                                        <div class="col-md-12">
                                            <select class="form-select" name="question_id" id="question_id">
                                                <option>Select</option>
                                                @foreach($data as $d)
                                                    <option value="{{$d->id}}">{{$d->subject}}</option>
                                                @endforeach
                                            </select>                                            
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Title</label>
                                        <input type="text" class="form-control" name="title" placeholder="Enter Title" />
                                        <span class="text-danger" id="tilteError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Option1</label>
                                        <input type="text" class="form-control" id="option" name="option[]" placeholder="Enter Option1" />
                                        <input type="radio" id="html" name="correct_answer" value="1">
                                        <span class="text-danger" id="featuresError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Option2</label>
                                        <input type="text" class="form-control" id="option" name="option[]" placeholder="Enter Option2" />
                                        <input type="radio" id="html" name="correct_answer" value="1">
                                        <span class="text-danger" id="featuresError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Option3</label>
                                        <input type="text" class="form-control" id="option" name="option[]" placeholder="Enter Option3" />
                                        <input type="radio" id="html" name="correct_answer" value="1">
                                        <span class="text-danger" id="featuresError"></span></br>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Option4</label>
                                        <input type="text" class="form-control" id="option" name="option[]" placeholder="Enter Option4" />
                                        <input type="radio" id="html" name="correct_answer" value="1">
                                        <span class="text-danger" id="featuresError"></span></br>
                                    </div>

                                    <div>
                                        <div>
                                            <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light me-1">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- container-fluid -->
        </div>
    </div>
</div>
<!-- END layout-wrapper -->
@endsection

@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("admin.answer.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("admin.answer.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $("#featuresIdError").text(result.responseJSON.errors.features_id);
                    $("#featuresError").text(result.responseJSON.errors.sub_features);
                }
            })
        });
    })
</script>

@endpush