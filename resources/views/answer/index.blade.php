@extends('layouts.admin.master')
@section('title','Answer')
@section('content')
@push('css')
    <style>
        .error{
            color:red;
        }
    </style>
@endpush
<!-- Begin page -->
<div id="layout-wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">Answer</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Property</a></li>
                                    <li class="breadcrumb-item active">Answer</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Features Update</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form id="editform">
                                    @csrf
                                    <div class="mb-3">
                                        <label class="form-label">Question</label>
                                        <input type="text" class="form-control" name="features" id="features" placeholder="Enter Question" />
                                        <span class="text-danger" id="FeaturesError"></span></br>
                                    </div>
                            </div>
                            <input type="hidden" id="id" name="id">
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" id="update" name="update" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.answer.create')}}"  id="update" name="update" class="btn btn-primary"><i class="fa fa-plus"></i>Add Answer</a>
                                {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page-content -->
    </div>
</div>
<!-- END layout-wrapper -->


@endsection
@push('js')
{!! $dataTable->scripts() !!}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).on("click", ".btnedit", function(e) {
        e.preventDefault();
        var id = $(this).attr("href");
        $.ajax({
            url: id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#id").val(data.features.id);
                $("#features").val(data.features.features);
            },
        });
    });


    $(document).on("click",".btndelete",function(e){
        e.preventDefault();
        var id = $(this).attr("data-id");
        $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            url:"",
            type:'post',
            data:{id:id},
            dataType:"JSON",
            success:function(data){
                swal({
                    title: 'Delete',
                    text: 'Are you sure delete succesfully ??',
                    buttons: ['Cancel', 'Delete']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['propertyfeatures-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            }
        }); 
    });

    //Update
    $("#update").click(function(e) {
        e.preventDefault();
        var form = $("#editform")[0];
        var data = new FormData(form);
        $.ajax({
            url: '{{route("admin.answer.update")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function() {
                swal({
                    title: 'Updated',
                    text: 'Updated Succesfully',
                    buttons: ['Cancel', 'Update']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['propertyfeatures-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            },
            error: function(data) {
                $.each(data.responseJSON.errors,function(key,value){
                    $('input[name='+ key +']').after('<span class="error">'+ value +'</span>');
                });
            }
        }); 
        $("#myModal").on('hidden.bs.modal', function() {
            $('.error').empty();
        });
    });
   
</script>
@endpush