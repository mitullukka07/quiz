@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">{{ __('MCQ') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <!-- {{ __('You are logged in!') }} -->

                    <form class="custom-validation" id="myform" method="post">
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <div class="step-detail-box">
                                <!-- <h4 class="step-main-head">MCQ Test</h4> -->
                                <div class="filters-boxes">
                                    <div class="filter-box">
                                        <div class="row">

                                            <!-- @php
                                            $data = DB::table('questions')->get();
                                            @endphp

                                            @foreach($data as $d)
                                            @php
                                                $data1 = DB::table('answers')->where('question_id',$d->id)->get();
                                            @endphp

                                            <div class="col-lg-4" style="width:100%">
                                                <input type="hidden" value="{{$d->right_answer}}" name="right_answer[]">
                                                <input type="hidden" name="question_id[]" value="{{$d->id}}">

                                                <h5 class="filter-head" value="{{$d->id}}">
                                                    <li>{{$d->title}}</li>
                                                </h5>
                                                <div class="radiocheckx mb-4 mb-lg-0">
                                                    <div class="custom-control custom-checkbox mb-4">
                                                        @foreach($data1 as $d1)
                                                        <input type="radio" value="{{$d1->id}}" name="answer_id[{{$d->id}}]">
                                                        <label>{{$d1->answer}}</label>
                                                        <br>
                                                        <br>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- @endforeach -->
                                            @foreach($find as $d)
                                            <div class="col-lg-4" style="width:100%">
                                                <h5 class="filter-head" value="{{$d->id}}" name="question_id[{{$d->id}}]">
                                                    {{$d->title}}
                                                </h5>

                                                @php
                                                    $data1 = DB::table('answers')->where('question_id',$d->id)->get();
                                                @endphp

                                                @foreach($data1 as $d1)
                                                <div class="radiocheckx mb-4 mb-lg-0">
                                                    <div class="custom-control custom-checkbox mb-4">
                                                        <input type="radio" name="answer_id[{{$d->id}}]" value="{{$d1->id}}">
                                                        <label>{{$d1->answer}}</label>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>

                                            @endforeach
                                            <button type="button" id="submit" class="btn btn-primary waves-effect waves-light me-1">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {

                    toastr.success('Submit Successfully');
                    window.location.href = '{{route("result")}}';

                },
            })
        });
    })
</script>
@endpush