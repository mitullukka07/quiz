@extends('layouts.admin.master')
@section('title','User')
@section('content')
@push('css')
<style>
    .error {
        color: red;
    }
</style>
@endpush
<!-- Begin page -->
<div id="layout-wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">User List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Quiz</a></li>
                                    <li class="breadcrumb-item active">User List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">View Subject</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>

                            <table class="table table-border">
                                <div class="modal-body">
                                    <form id="editform">
                                        @csrf
                                        <div class="mb-3">
                                            <label class="form-label">Subject</label>
                                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Enter Subject" />
                                            <span class="text-danger" id="featuresError"></span></br>
                                        </div>
                                </div>
                            </table>
                            <input type="hidden" id="id" name="id">
                            </form>
                            <div class="modal-footer">
                                <button type="button" id="update" class="btn btn-primary" data-bs-dismiss="modal">Update</button>
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{route('admin.subject.create')}}" id="update" name="update" class="btn btn-primary"><i class="fa fa-plus"></i>Add Subject</a>
                                {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page-content -->
    </div>
</div>
<!-- END layout-wrapper -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@endsection
@push('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('description');
    var desc = CKEDITOR.instances.description.on("change", function() {
        CKEDITOR.instances.description.updateElement();
    });
</script>
<script>
    $(document).on("click", ".btnedit", function(e) {
        e.preventDefault();
        var id = $(this).attr("href");
        $.ajax({
            url: id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#id").val(data.subject.id);
                $("#subject").val(data.subject.subject);
            },
        });
    });


    //Update
    $("#update").click(function(e) {
        e.preventDefault();
        var form = $("#editform")[0];
        var data = new FormData(form);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route("admin.subject.update")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function() {
                swal({
                    title: 'Updated',
                    text: 'Updated Succesfully',
                    buttons: ['Cancel', 'Update']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['subject-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            },
            error: function(data) {
                $.each(data.responseJSON.errors, function(key, value) {
                    $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                });
            }
        });
        $('#myModal').on('hidden.bs.modal', function() {
        $(".error").empty();
    });
    return false;
    });

    
</script>
@endpush