@extends('layouts.admin.master')
@section('title','User')
@section('content')
@push('css')
<style>
    .error {
        color: red;
    }
</style>
@endpush
<!-- Begin page -->
<div id="layout-wrapper">
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0">Result List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Quiz</a></li>
                                    <li class="breadcrumb-item active">User List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">View User</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            
                            <table class="table table-border">
                                <div class="modal-body">
                                    <form id="showform">
                                        @csrf
                                        <tr>
                                            <th><b>Name</b></th>
                                            <td><span id="set_name"></span></td>
                                        </tr>

                                        <tr>
                                            <th><b>Email</b></th>
                                            <td><span id="set_email"></span></td>
                                        </tr>
                                </div>
                            </table>
                            <input type="hidden" id="id" name="id">
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page-content -->
    </div>
</div>
<!-- END layout-wrapper -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@endsection
@push('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('description');
    var desc = CKEDITOR.instances.description.on("change", function() {
        CKEDITOR.instances.description.updateElement();
    });
</script>
<script>
    $(document).on("click", '.btnshow', function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#set_name").text(data.user.name);
                $("#set_email").text(data.user.email);
            }
        });
    });

</script>
@endpush

