@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">{{ __('MCQ Subject Select') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <!-- {{ __('You are logged in!') }} -->

                    <form class="custom-validation" action="{{route('subject')}}" method="post" id="myform">
                        @csrf
                        <div class="tab-pane">
                            @php
                            $data = DB::table('answers')->get();
                            @endphp
                            @foreach($data as $q)
                            <input type="hidden" name="question_id[]" value="{{$q->question_id}}">
                            @endforeach
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            @php
                            $data = DB::table('subjects')->get();
                            @endphp
                            <div class="mb-3">
                                <label class="col-md-2 col-form-label">Subject</label>
                                <div class="col-md-12">
                                    <select class="form-select" name="subject_id" id="subject_id">
                                        <option>Select</option>
                                        @foreach($data as $d)
                                        <option option value="{{$d->id}}" data-id="{{$d->id}}">{{$d->subject}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light me-1">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection