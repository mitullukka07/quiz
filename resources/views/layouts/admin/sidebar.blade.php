<div class="vertical-menu">

    <!-- LOGO -->
    <div class="navbar-brand-box">
        <a href="index.html" class="logo logo-dark">
            <span class="logo-sm">
                <img src="assets/images/logo-sm.png" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{asset('assets/images/logo-dark.png')}}" alt="" height="20">
            </span>
        </a>

        <a href="index.html" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{asset('assets/images/logo-sm.png')}}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{asset('assets/images/logo-light.png')}}" alt="" height="20">
            </span>
        </a>
    </div>

    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
        <i class="fa fa-fw fa-bars"></i>
    </button>

    <div data-simplebar class="sidebar-menu-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="fas fa-desktop"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.user.index') }}">
                        <i class="fas fa-user"></i>
                        <span>User</span>
                    </a>
                </li>

                <!-- <li>
                <a href="{{ route('admin.result.show') }}">
                    <i class="fas fa-question-circle"></i>
                    <span>Attempt Question</span>
                </a>
            </li> -->

                <!-- <li>
                <a href="{{ route('admin.question.index') }}">
                    <i class="fas fa-pen"></i>
                    <span>Question</span>
                </a>
            </li> -->

                <li>
                    <a href="{{ route('admin.subject.index') }}">
                        <i class="fas fa-pen"></i>
                        <span>Subject</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.answer.index') }}">
                        <i class="fas fa-pen"></i>
                        <span>Answer</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>